# Lumen PHP Framework Starter

## Additions 
### JWT
With [jwt-auth](https://github.com/tymondesigns/jwt-auth) and the following API routes:
- `register`
- `login`
- `profile`

### Dev environment with Docker
This starter provide a `docker-compose.yml` with 2 services:
- [ninobysa/laravel-pg](https://hub.docker.com/r/ninobysa/laravel-pg) a docker image ready for Laravel and pgsql.
- postgres (without volumes)

Of course, you can use [Laravel Homestead](http://laravel.com/docs/homestead) instead of Docker.

## Installation
``` sh
git clone --depth=1 --branch=master https://gitlab.com/nin_/lumen-starter.git my-project && cd my-project
cp .env.example .env 
composer update
docker-compose up -d
docker-compose exec app php artisan jwt:secret
```

## Usage
Follow the [Lumen documentation](https://lumen.laravel.com/docs/7.x/installation)

## Licenses

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

The additions from this starter are licensed under the [The Unlicense](https://unlicense.org/).
It means that you can use them all you like and don't even mention this starter.

