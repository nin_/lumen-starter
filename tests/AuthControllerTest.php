<?php

use App\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Testing\DatabaseMigrations;

class AuthControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function testRegister()
    {
        $this->post('api/register', ['name' => 'tester', 'password' => 'a_secret']);

        $this->assertResponseStatus(201);
        $this->seeJsonStructure(['data' => ['name', 'created_at']]);
        $this->assertSame('tester', json_decode($this->response->getContent())->data->name);
    }

    public function testLogin()
    {
        $user = User::factory()->create(['password' => Hash::make('a_secret')]);
        $this->post('api/login', ['name' => $user->name, 'password' => 'a_secret']);

        $this->assertResponseOk();
        $this->seeJsonStructure(['token', 'token_type', 'expires_in']);
    }

    /**
     * @depends testLogin
     */
    public function testGetProfile()
    {
        $user = User::factory()->create(['password' => Hash::make('a_secret')]);
        $this->post('api/login', ['name' => $user->name, 'password' => 'a_secret']);
        $token = json_decode($this->response->getContent())->token;

        $this->get('api/profile', ['Authorization' => $token]);

        $this->seeJsonStructure(['data' => ['name', 'email', 'created_at']]);
        $this->assertSame($user->name, json_decode($this->response->getContent())->data->name);
        $this->assertSame($user->email, json_decode($this->response->getContent())->data->email);
    }

    public function testGetProfileUnauthenticated()
    {
        $this->get('api/profile');
        $this->assertResponseStatus(401);
    }
}
