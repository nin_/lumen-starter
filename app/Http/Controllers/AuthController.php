<?php

namespace App\Http\Controllers;

use App\Http\Resources\User as UserResource;
use App\Http\Validators\ValidateAuth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use ValidateAuth;

    public function login(Request $request)
    {
        $this->validateLogin($request);
        $credentials = $request->only(['name', 'password']);

        if (!$token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function register(Request $request)
    {
        $this->validateRegister($request);

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);

        return (new UserResource($user))->response()->header('Status', 201);
    }
}
