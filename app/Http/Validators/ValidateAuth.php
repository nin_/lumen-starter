<?php

namespace App\Http\Validators;

use Illuminate\Http\Request;

trait ValidateAuth
{
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required',
        ]);
    }

    protected function validateRegister(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:64|unique:users,name',
            'password' => 'required|min:8',
            'email' => 'email|max:255|unique:users,email',
        ]);
    }
}
