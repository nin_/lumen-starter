<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class User extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->when(Auth::user() && Auth::user()->id === $this->id, $this->email),
            'created_at' => $this->created_at,
        ];
    }
}
